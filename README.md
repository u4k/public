# README #

### What is this repository for? ###

このリポジトリは、私の成果物について、外部からのコメントや要望を発信していただくことを目的として、公開しています。

私の成果物は、自己責任の下、ご使用ください。

### How do I get set up? ###

開発環境: Visual Studio 2008 SP1

開発言語: C++ (MFC)

検証環境: Windows 7 Pro SP1 64Bit

### Project List ###

[FolderDialog_Lib] CFileDialogの外観を持つ、フォルダを開くためのダイアログ