#pragma once

// CFolderDialog

class CFolderDialog : public CFileDialog
{
	DECLARE_DYNAMIC(CFolderDialog)

private:
	static HHOOK m_hook;

	class CWndEx *m_button;
	class CWndEx *m_combobox;

private:
	static LRESULT CALLBACK HookKeyboardProc(int nCode, WPARAM wp, LPARAM lp);

public:
	char m_folderPath[MAX_PATH];
	char m_folderName[MAX_PATH];

	BOOL OnCheck(enum SOURCE source);

public:
	CFolderDialog(LPCTSTR lpszFilter = NULL
					, CWnd* pParentWnd = NULL);

	virtual ~CFolderDialog();

protected:
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	virtual BOOL OnInitDialog();
	virtual BOOL OnFileNameOK();

public:
	virtual INT_PTR DoModal();

protected:
	DECLARE_MESSAGE_MAP()
};

