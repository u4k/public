
#include "stdafx.h"
#include "gpSubClass.h"

#include "FolderDialog.h" // CFolderDialog

BEGIN_MESSAGE_MAP(CWndEx, CWnd)
	//{{AFX_MSG_MAP(CWndEx)
 	ON_WM_LBUTTONUP()
	ON_WM_KEYDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

LRESULT CWndEx::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
// 	switch(message){
// 		case WM_SYSKEYDOWN: TRACE("WM_SYSKEYDOWN\n", message); break;
// 		case WM_SYSKEYUP: TRACE("WM_SYSKEYUP\n", message); break;
// 		case WM_KEYDOWN: TRACE("WM_KEYDOWN\n", message); break;
// 		case WM_KEYUP: TRACE("WM_KEYUP\n", message); break;
// 		case WM_LBUTTONUP: TRACE("WM_LBUTTONUP\n", message); break;
// 		case WM_DESTROY: TRACE("WM_DESTROY\n", message); break;
// 		case WM_NCDESTROY: TRACE("WM_NCDESTROY\n", message); break;
// 	}

	return CWnd::WindowProc(message, wParam, lParam);
}

BOOL CALLBACK CWndEx::EnumChildProc(HWND hWnd, LPARAM lParam)
{
	CWnd *window = CWnd::FromHandle(hWnd);

	if(strcmp(window->GetRuntimeClass()->m_lpszClassName, "CFolderDialog") == 0){
		*((CWnd**)lParam) = window;
		return FALSE;
	}

	return TRUE;
}

void CWndEx::OnCheck() 
{
	CFolderDialog *dialog;
	EnumChildWindows(m_base->GetSafeHwnd(), EnumChildProc, (LPARAM)&dialog);

	if(dialog->OnCheck(SR_OTHER) == TRUE){
		((CDialog*)m_base)->EndDialog(IDOK);
	}
}

void CWndEx::OnLButtonUp(UINT nFlags, CPoint point) 
{
	if(m_item == IT_BUTTON){ // Button
		CWndEx::OnCheck();
	}

 	CWnd::OnLButtonUp(nFlags, point);
}

void CWndEx::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if(m_item == IT_COMBOBOX && (nChar == VK_RETURN)){ // "Enter Key to Combobox" or "Spase Key to Button"
		TRACE("OnKeyDown(%d, %02X)\n", m_item, nChar);
		CWndEx::OnCheck();
	}

	CWnd::OnKeyDown(nChar, nRepCnt, nFlags);
}

