// FolderDialog.cpp : 実装ファイル
//

#include "stdafx.h"
#include "FolderDialog.h"

HHOOK CFolderDialog::m_hook = NULL;

// CFolderDialog

IMPLEMENT_DYNAMIC(CFolderDialog, CFileDialog)

CFolderDialog::CFolderDialog(LPCTSTR lpszFilter, CWnd *pParentWnd)
	: CFileDialog(TRUE, "", "", OFN_HIDEREADONLY, lpszFilter, pParentWnd, 0, FALSE)
{
	strcpy_s(m_folderName, "");
	strcpy_s(m_folderPath, "");

	m_hook = SetWindowsHookEx(WH_KEYBOARD, HookKeyboardProc, NULL, GetCurrentThreadId());
}

CFolderDialog::~CFolderDialog()
{
	UnhookWindowsHookEx(m_hook);
}

BEGIN_MESSAGE_MAP(CFolderDialog, CFileDialog)
END_MESSAGE_MAP()

LRESULT CALLBACK CFolderDialog::HookKeyboardProc(int nCode, WPARAM wp, LPARAM lp)
{
	if(nCode >= 0 && GetKeyState(VK_SHIFT) < 0 && (wp == VK_OEM_1 || wp == VK_OEM_2)){ // '*' or '?'
		TRACE("破棄 = %X, %X\n", nCode, wp);
		return TRUE;
	}

//	TRACE("有効 = %X, %X\n", nCode, wp);
	return CallNextHookEx(m_hook, nCode, wp, lp);
}

// CFolderDialog メッセージ ハンドラ

LRESULT CFolderDialog::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	if(message == WM_DESTROY){
		m_button->UnsubclassWindow();
		m_combobox->UnsubclassWindow();

		delete m_button;
		delete m_combobox;
	}

	return CFileDialog::WindowProc(message, wParam, lParam);
}

BOOL CFolderDialog::OnInitDialog()
{
	CFileDialog::OnInitDialog();

	GetParent()->SetWindowText("フォルダの選択");
	GetParent()->SetDlgItemText(stc3, "フォルダ名(&N):");
	GetParent()->SetDlgItemText(stc4, "フォルダの場所(&I):");

	m_button = new CWndEx();
	m_button->SubclassDlgItem(IDOK, GetParent());
	m_button->m_item = IT_BUTTON;
	m_button->m_base = GetParent();
	m_button->SetWindowText("フォルダ選択(&O)");

	m_combobox = new CWndEx();
	m_combobox->SubclassWindow(((CComboBoxEx*)GetParent()->GetDlgItem(cmb13))->GetEditCtrl()->GetSafeHwnd());
	m_combobox->m_item = IT_COMBOBOX;
	m_combobox->m_base = GetParent();

	return TRUE;
}

INT_PTR CFolderDialog::DoModal()
{
	INT_PTR result;

	if((result = CFileDialog::DoModal()) == IDOK){
		strcpy_s(m_szFileName, (CString)m_folderPath + ((strcmp(m_folderName, "") != 0) ? (CString)"\\" + m_folderName : ""));
	}

	return result;
}

// ファイル名もしくはフォルダ名をチェックする関数
const TCHAR* GetInvalidPathNameChars(LPCTSTR pszPathName // チェックするファイル名へのポインタです
									 , bool fFolder = false // true の場合フォルダ名として判定します	
									 , bool fWildcard = false) // true の場合ワイルドカードを有効とみなします							 
{
	// 空ではないか
	if(pszPathName == NULL || pszPathName[0] == '\0'){
		return "\\0";
	}

	if(fFolder == false){
		// デバイスファイルではないか
		int Length = lstrlen(pszPathName);

		if(Length == 3){
			static const LPCTSTR pszDevices[] = {TEXT("CON"), TEXT("PRN"), TEXT("AUX"), TEXT("NUL")};

			for(int i = 0; i < _countof(pszDevices); i++){
				if(lstrcmpi(pszDevices[i], pszPathName) == 0){
					return pszPathName;
				}
			}
		} else if(Length == 4){
			TCHAR szName[5];

			for(int i = 1; i <= 9; i++){
				wsprintf(szName, TEXT("COM%d"), i);

				if(lstrcmpi(szName, pszPathName) == 0){
					return pszPathName;
				}
			}

			for(int i = 1 ; i <= 9; i++){
				wsprintf(szName, TEXT("LPT%d"), i);

				if(lstrcmpi(szName, pszPathName) == 0){
					return pszPathName;
				}
			}
		}
	}

#ifdef UNICODE
	typedef WORD UTCHAR;
#else
	typedef BYTE UTCHAR;
#endif

	LPCTSTR p = pszPathName;

	while(*p != _T('\0')){
		TCHAR c = *p;

		// 使用できない文字ではないか
		if((UTCHAR)c <= 31 || c == _T('<') || c == _T('>') || c == _T(':')
			|| c == _T('"') || c == _T('/') || c == _T('\\') || c == _T('|')
			|| (fWildcard == false && (c == _T('*') || c == _T('?')))){
			
			return p;
		}

		// 半角スペースか . で終了していないか
		if((c == _T(' ') || c == _T('.')) && *(p + 1) == _T('\0')){
			return p;
		}

#ifndef UNICODE
		// 2バイト文字のチェック
		if(IsDBCSLeadByteEx(CP_ACP, c) == TRUE){
			// 2バイト文字が途中で切れていないか
			if(*(++p) == _T('\0')){
				return "\\0";
			}
		}
#endif
		++p;
	}

	return "";
}

BOOL CFolderDialog::OnCheck(SOURCE source)
{
	GetParent()->SendMessage(CDM_GETSPEC, (WPARAM)MAX_PATH, (LPARAM)m_folderName);

	if(strcmp(m_folderName, "") != 0){
		if(source == SR_OTHER){
			TRACE("OnCheck(FALSE)\n");
			return FALSE;
		} else {
			GetParent()->SendMessage(CDM_GETFOLDERPATH, (WPARAM)MAX_PATH, (LPARAM)m_folderPath); // 表示されているフォルダのパス
		}
	} else {
		GetParent()->SendMessage(CDM_GETFILEPATH, (WPARAM)MAX_PATH, (LPARAM)m_folderPath); // 選択されているファイルパスもしくはフォルダパス

// 		// 選択されているパスが実在していない（選択後に削除）場合は、表示されているフォルダのパスを取得
// 		if(PathFileExists(m_folderPath) != TRUE){
// 			GetParent()->SendMessage(CDM_GETFOLDERPATH, (WPARAM)MAX_PATH, (LPARAM)m_folderPath);
// 		}

		// 実在するファイルを選択することが不可能な「はず」なので、m_folderPathがディレクトリであるかチェックしなくてよい「はず？」
		// 選択されているパスが実在していない、もしくはフォルダパスではない場合は、表示されているフォルダのパスを取得
		if(PathFileExists(m_folderPath) != TRUE || PathIsDirectory(m_folderPath) != FILE_ATTRIBUTE_DIRECTORY){
			GetParent()->SendMessage(CDM_GETFOLDERPATH, (WPARAM)MAX_PATH, (LPARAM)m_folderPath);
		}
	}

	// 実在するパスなのに、フォルダを示していないのであれば無効なパス
	CString temp = m_folderPath + ((strcmp(m_folderName, "") != 0) ? (CString)"\\" + m_folderName : "");
	if((PathFileExists(m_folderPath) == TRUE && PathIsDirectory(m_folderPath) != FILE_ATTRIBUTE_DIRECTORY)
		|| (PathFileExists(temp) == TRUE && PathIsDirectory(temp) != FILE_ATTRIBUTE_DIRECTORY)){
		
		return FALSE;
	}

	// フォルダ名が空ではないのに、フォルダパスに使用できない文字が存在する場合は無効なパス
	if(strcmp(m_folderName, "") != 0 && strcmp(GetInvalidPathNameChars(m_folderName, true), "") != 0){
		return FALSE;
	}

	return TRUE;
}

BOOL CFolderDialog::OnFileNameOK()
{	
	TRACE("OnFileNameOK\n");

	return (OnCheck(SR_SELF) == TRUE) ? FALSE : TRUE;
}

