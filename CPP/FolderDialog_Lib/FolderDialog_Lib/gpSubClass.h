
#pragma once

#include <dlgs.h> // stc3, stc4, cmb13
#include <shlwapi.h> // PathFileExists, PathIsDirectory
//shlwapi.lib

#include <afxcmn.h> // CComboBoxEx

enum ITEM { IT_BUTTON = 0
			, IT_COMBOBOX };

enum SOURCE { SR_SELF = 0
			, SR_OTHER };

class CWndEx : public CWnd
{
private:
	static BOOL CALLBACK EnumChildProc(HWND hWnd, LPARAM lParam);

private:
	void OnCheck();

public:
	ITEM m_item;
	CWnd *m_base;

protected:
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);

protected:
	//{{AFX_MSG(CWndEx)
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};